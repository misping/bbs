<?php

namespace App\Application\Content;

use App\Application\Channel\Models\Channel;
use App\Application\Content\Models\Content;
use Composer\Http\Controller;
use Illuminate\Validation\Rule;
use Spatie\QueryBuilder\AllowedFilter;

class Client extends Controller
{
    public function __construct(Content $model)
    {
        $this->model = $model;
        $this->defaultSorts = "id";
        $this->allowedSorts = ['-content_num', '-id'];
        $this->allowedFilters = [
            'title',
            "type"
        ];
        $this->validateRules = [
            'title' => 'required',
            'channel_id' => 'required',
        ];
        $this->validateMessage = [
            'title.required' => '内容标题必须填写',
            'channel_id.required' => '频道必须填写',
        ];
    }
}
