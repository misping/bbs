<?php

namespace App\Application\Content\Models;

use App\Application\Comment\Models\Comment;
use DateTimeInterface;
use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    protected $table = 'bbs_content';

    protected $fillable = [
        'id',
        'channel_id',
        'user_id',
        'title',
        'give_num', //点赞
        'recovery_num', //恢复数
        'comment_num' //评论数
    ];
    protected $appends = ['last_comment_time', 'user_num'];
    public function getLastCommentTimeAttribute()
    {
        return Comment::where("content_id", $this->id)->orderBy("id", 'desc')->value("created_at");
    }
    public function getUserNumAttribute()
    {
        return Comment::where("content_id", $this->id)->distinct("user_id")->count();
    }
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
