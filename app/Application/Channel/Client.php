<?php

namespace App\Application\Channel;

use App\Application\Channel\Models\Channel;
use Composer\Http\Controller;
use Spatie\QueryBuilder\AllowedFilter;

class Client extends Controller
{
    public function __construct(Channel $hannel)
    {
        $this->model = $hannel;
        $this->allowedSorts = ['-content_num', '-id'];
        $this->allowedFilters = [
            'title',

        ];
        $this->validateRules = [
            'title' => 'required',
        ];
        $this->validateMessage = [
            'required' => '频道名称必填',
        ];
    }
}
