<?php

namespace App\Application\Like;


use App\Application\Like\Models\Like;
use Composer\Exceptions\ApiErrorCode;
use Composer\Exceptions\ApiException;
use Composer\Http\Controller;
use Illuminate\Support\Facades\Auth;

class Client extends Controller
{
    protected $mainModel;
    public $type;
    public function __construct(Like $model)
    {
        $this->model = $model;
        $this->validateRules = [
            'relation_id' => 'required',
        ];
        $this->validateMessage = [
            'relation_id.required' => '点赞id必传',

        ];
    }
    public function performCreate()
    {

        parent::performUpdate();
        $user = Auth::guard("platform")->user();
        $this->data['user_id']  = $user->id;
        $where = [
            'user_id' => $user->id,
            'type' =>  $this->type,
            'relation_id' => $this->data['relation_id']
        ];
        $exists = $this->model->where($where)->exists();
        if ($exists) {
            throw new ApiException('只能点赞一次', ApiErrorCode::VALIDATION_ERROR);
        }
        $this->data['type'] = $this->type;

        $exists  = $this->mainModel->where(['id' => $this->data['relation_id']])->exists();
        if (!$exists) {
            throw new ApiException('relation_id 点赞关联id不存在', ApiErrorCode::VALIDATION_ERROR);
        }
    }
}
