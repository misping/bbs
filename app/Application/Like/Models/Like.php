<?php

namespace App\Application\Like\Models;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    protected $table = 'bbs_like';

    protected $fillable = [
        'id',
        'type', //1 是内容 2 评论
        'user_id',
        'relation_id',

    ];
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
