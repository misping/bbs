<?php

namespace App\Application\Comment;


use App\Application\Comment\Models\Reply;
use Composer\Http\Controller;


class ReplyClient extends Controller
{
    public function __construct(Reply $model)
    {
        $this->model = $model;

        $this->allowedSorts = ['-recovery_num', '-id'];
        $this->allowedFilters = [
            'title',
        ];
        $this->validateRules = [
            'comment' => 'required',
            'comment_id' => 'required',
        ];
        $this->validateMessage = [
            'comment.required' => '内容标题必须填写',
            'comment_id.required' => '评论id必须填写',
        ];
    }
}
