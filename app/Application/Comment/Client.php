<?php

namespace App\Application\Comment;


use App\Application\Comment\Models\Comment;
use Composer\Http\Controller;


class Client extends Controller
{
    public function __construct(Comment $model)
    {
        $this->model = $model;
        $this->defaultSorts = "id";
        $this->allowedSorts = ['-content_num', '-id'];
        $this->allowedFilters = [
            'title',
        ];
        $this->validateRules = [
            'comment' => 'required',
            'content_id' => 'required',
        ];
        $this->validateMessage = [
            'comment.required' => '内容标题必须填写',
            'content_id.required' => '内容id必须填写',
        ];
    }
}
