<?php

namespace App\Application\Auth;


use Composer\Application\Auth\UserClient as BaseUserClient;
use Composer\Exceptions\ApiErrorCode;
use Composer\Exceptions\ApiException;

class UserClient extends BaseUserClient
{

    public function performDelete()
    {
        if ($this->model::findOrFail($this->id)['is_admin'] == '1') {
            throw new ApiException('不能删除超级管理员', ApiErrorCode::VALIDATION_ERROR);
        }
    }
}
