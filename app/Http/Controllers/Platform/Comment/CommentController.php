<?php

namespace App\Http\Controllers\Platform\Comment;

use App\Application\Channel\Models\Channel;
use App\Application\Comment\Client;
use App\Application\Comment\Models\Comment;
use App\Application\Content\Models\Content;
use Composer\Exceptions\ApiErrorCode;
use Composer\Exceptions\ApiException;
use Illuminate\Support\Facades\Auth;
use Spatie\QueryBuilder\AllowedFilter;

class CommentController extends Client
{

    public function __construct(Comment $model)
    {
        parent::__construct($model);
        $this->allowedSorts = ['-recovery_num'];
        $this->allowedFilters = [
            'title',
            AllowedFilter::exact('content_id'),
        ];
        $this->allowedIncludes = ['infoReply'];
    }
    public function performBuildFilterList()
    {
    }
    public function performCreate()
    {
        parent::performCreate();
        // $user = auth()->user("platform");
        $user = Auth::guard("platform")->user();
        $contentId = $this->data['content_id'];
        $content = Content::where("id", $contentId)->exists();
        if (!$content) {
            throw new ApiException('内容id不存在', ApiErrorCode::VALIDATION_ERROR);
        }
        $this->data['user_id'] = $user->id;
        $this->data['recovery_num'] = 0;
        $this->data['give_num'] = 0;
    }

    public function afterCreate()
    {
        $contentId = $this->data['content_id'];
        if ($this->row) {
            Content::where("id", $contentId)->increment("comment_num");
        }
    }
}
