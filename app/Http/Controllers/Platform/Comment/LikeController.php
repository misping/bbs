<?php

namespace App\Http\Controllers\Platform\Comment;

use App\Application\Comment\Models\Comment;
use App\Application\Like\Client;
use App\Application\Like\Models\Like;

class LikeController extends Client
{
    public function __construct(Like $like)
    {
        parent::__construct($like);
        $this->mainModel = new Comment;
        $this->type = 2;
    }
    public function afterCreate()
    {
        $id = $this->data['relation_id'];
        if ($this->row) {
            Comment::where("id", $id)->increment("give_num");
        }
    }
}
