<?php

namespace App\Http\Controllers\Platform\Comment;

use App\Application\Comment\Models\Comment;
use App\Application\Comment\ReplyClient;

use App\Application\Content\Models\Content;
use Composer\Exceptions\ApiErrorCode;
use Composer\Exceptions\ApiException;
use Illuminate\Support\Facades\Auth;
use Spatie\QueryBuilder\AllowedFilter;

class ReplyController extends ReplyClient
{


    public function performBuildFilterList()
    {
    }
    public function performCreate()
    {
        parent::performCreate();

        $user = Auth::guard("platform")->user();
        $commentId = $this->data['comment_id'];
        $contentId = Comment::where("id", $commentId)->value("content_id");
        if (!$contentId) {
            throw new ApiException('评论id不存在', ApiErrorCode::VALIDATION_ERROR);
        }
        $this->data['user_id'] = $user->id;
        $this->data['content_id'] = $contentId;
    }

    public function afterCreate()
    {
        $contentId = $this->data['comment_id'];
        if ($this->row) {
            Comment::where("id", $contentId)->increment("recovery_num");
        }
    }
}
