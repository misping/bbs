<?php

namespace App\Http\Controllers\Platform\Content;

use App\Application\Content\Models\Content;
use App\Application\Like\Client;
use App\Application\Like\Models\Like;

class LikeController extends Client
{
    public function __construct(Like $like)
    {
        parent::__construct($like);
        $this->mainModel = new Content;
        $this->type = 1;
    }



    public function afterCreate()
    {
        $id = $this->data['relation_id'];
        if ($this->row) {
            Content::where("id", $id)->increment("give_num");
        }
    }
}
