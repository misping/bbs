<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBbsContentTable extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('bbs_content', function (Blueprint $table) {
            $table->id();
            $table->integer('channel_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->string('title');
            $table->integer('give_num')->unsigned();
            $table->integer('comment_num');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bbs_content');
    }
}
