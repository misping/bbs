<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBbsCommentTable extends Migration
{
    /**
     * Run the migrations.
     *   'id',
  
     * @return void
     */
    public function up()
    {
        Schema::create('bbs_comment', function (Blueprint $table) {
            $table->id();

            $table->integer("user_id");
            $table->text("comment");
            $table->integer("content_id");
            $table->integer("recovery_num");
            $table->integer("give_num");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bbs_comment');
    }
}
