<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBbsChannelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bbs_channel', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->integer('user_num')->default(0);
            $table->integer('content_num')->default(0);
            // $table->integer('content_num')->default(0);
            $table->timestamps();
            // $table->comment('bbs-频道');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bbs_channel');
    }
}
