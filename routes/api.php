<?php

use AlibabaCloud\Acm\V20200206\Roa;
use Composer\Support\Crypt\AES;
use Composer\Support\Crypt\Rsa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::post("get", function () {

    $key['iv'] = "HVHHU7xxPVREgnuy";
    $key['key'] = "Cd0QcLi8tk4fFwo8";
    $res['crypt_key'] = base64_encode(Rsa::publicEncrypt(json_encode($key), env("RSA_PUBLIC_KEY")));
    $res['password'] = AES::encode(request()->all()['password'], $key);
    return response()->json($res);
});
Route::group(['prefix' => "backend", 'namespace' => 'Backend',], function () {

    Route::group(['prefix' => 'auth', 'namespace' => 'Auth'], function () {

        // Route::get('current-user', 'AuthController@currentUser');

        Route::group(['prefix' => 'staff', 'middleware' => ['auth.admin']], function () {
            Route::group(['prefix' => 'user'], function () {
                Route::get('', 'UserController@getList');
                Route::get('{id}', 'UserController@get');
                Route::put('change/{id}', 'UserController@change');
                Route::put('{id}', 'UserController@update');
                Route::post('', 'UserController@create');
                Route::delete('{id}', 'UserController@delete');
            });
            Route::group(['prefix' => 'role'], function () {
                Route::get('', 'RoleController@getList');
                Route::get('select', 'RoleController@getSelect');
                Route::get('{id}', 'RoleController@get');
                Route::put('{id}', 'RoleController@update');
                Route::post('', 'RoleController@create');
                Route::delete('{id}', 'RoleController@delete');
            });
            Route::group(['prefix' => 'permission'], function () {
                Route::get('select', 'PermissionController@getSelect');
            });
        });
    });
    Route::group(['prefix' => 'channel', 'namespace' => 'Channel', 'middleware' => ['auth.permission:channel']], function () {
        Route::get('', 'ChannelController@getList');
        Route::get('{id}', 'ChannelController@get');
        Route::put('{id}', 'ChannelController@update');
        Route::post('', 'ChannelController@create');
        Route::delete('{id}', 'ChannelController@delete');
    });
    Route::group(['prefix' => 'content', 'namespace' => 'Content', 'middleware' => ['auth.permission:content']], function () {
        Route::get('', 'ContentController@getList');
        Route::get('{id}', 'ContentController@get');
        Route::put('{id}', 'ContentController@update');
        Route::post('', 'ContentController@create');
        Route::delete('{id}', 'ContentController@delete');
    });
    Route::group(['prefix' => 'comment', 'namespace' => 'Comment', 'middleware' => ['auth.permission:comment']], function () {
        Route::get('', 'CommentController@getList');
        Route::get('{id}', 'CommentController@get');
        Route::put('{id}', 'CommentController@update');
        Route::post('', 'CommentController@create');
        Route::delete('{id}', 'CommentController@delete');
    });
});;
Route::group(['prefix' => 'platform/wechat', 'namespace' => 'Platform\Wechat'], function () {
    Route::get('oauth', 'WechatController@oauth');
    Route::get('jssdk', 'JssdkController@get');
});
Route::group(['prefix' => "platform", 'namespace' => 'Platform', 'middleware' => ['auth:platform']], function () {

    Route::group(['prefix' => 'content', 'namespace' => 'Content'], function () {
        Route::get('', 'ContentController@getList');
        Route::post('', 'ContentController@create');
        Route::put('{id}', 'ContentController@update');
        Route::delete('{id}', 'ContentController@delete');
        Route::post('like', 'LikeController@create');
    });
    Route::group(['prefix' => 'comment', 'namespace' => 'Comment'], function () {
        Route::post('', 'CommentController@create');
        Route::post('reply', 'ReplyController@create');
        Route::get('', 'CommentController@getList');
        Route::post('like', 'LikeController@create');
    });
    Route::group(['prefix' => 'channel', 'namespace' => 'Channel'], function () {
        Route::get('', 'ChannelController@getList');
    });
});
